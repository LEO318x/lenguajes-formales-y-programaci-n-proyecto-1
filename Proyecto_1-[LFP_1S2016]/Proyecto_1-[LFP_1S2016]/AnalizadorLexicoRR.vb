﻿Public Class AnalizadorLexicoRR
    Dim cadena As String
    Dim tmpAlto, tmpAncho As String
    Dim anterior_alto = False, anterior_ancho = False
    Dim contadorColumnas = 1, contadorFilas = 1, pos = 0
    Dim caracter() As Char 'Letra que es analizada en el automata
    Dim token As String = "" 'Donde se va a concatenar la palabra valida
    Dim tokenTmp As String = ""
    Dim estado As Integer = -1
    Public Sub New(texto As String)
        Dim texto2 As String
        texto2 = texto.ToLower.Replace(vbCrLf, vbLf)
        Inicio(texto2)
    End Sub

    Public Sub Inicio(texto As String)
        cadena = texto
        caracter = cadena.ToCharArray
        'For i = 0 To cadena.Length - 1
        '    Console.WriteLine(Asc(caracter(i)))
        'Next
        'x0()
        x0()
    End Sub

    Public Sub x0()
        If (pos < cadena.Length) Then
            If (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X0 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(0)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X0 -> X0", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x0()
                End If

            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X0 -> X0", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x0()
            ElseIf (caracter(pos) = "{") Then
                Console.WriteLine("Fila:" + contadorFilas.ToString + "Columna: " + (contadorColumnas - (token.Length)).ToString)
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "{", "1", "AperturaLlaves")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X0 -> X1", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x1()
            Else
                'token += caracter(pos)
                'contadorColumnas += 1
                'pos += 1
                'x0()
                xError(0)
            End If
        Else
            'xError(0)
        End If
    End Sub

    Public Sub x1()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                Debugger("cambio", "X1 -> XS", 0)
                'token += caracter(pos)
                xS()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X1 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(1)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X1 -> X1", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x1()
                End If
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X1 -> X1", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x1()
            Else
                xError(1)
            End If
        Else
            'error
            'xError(1)
        End If
    End Sub

    Public Sub x2()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X2 -> X3", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x3()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X2 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(2)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X2 -> X2", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x2()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X2 -> X2", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x2()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X2 -> X2", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x2()
            Else
                'error
                xError(2)
            End If
        Else
            'error
            'xError(2)
        End If
    End Sub

    Public Sub x3()
        If (pos < cadena.Length) Then
            If (caracter(pos) = "{") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "{", "1", "AperturaLlaves")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X3 -> X4", 0)
                'Aceptacion
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x4()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X3 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(3)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X3 -> X3", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x3()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X3 -> X3", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x3()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X3 -> X3", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x3()
            Else
                xError(3)
            End If
        Else
            'error
            'xError(3)
        End If
    End Sub
    Public Sub x4()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                xS()
            ElseIf (caracter(pos) = "}") Then
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x30()
            ElseIf ((caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/")) Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X4 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(4)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X4 -> X4", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x4()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X4 -> X4", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x4()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X4 -> X4", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x4()
            Else
                xError(4)
            End If
        Else
            'error
            'xError(4)
        End If

    End Sub

    Public Sub x5()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X5 -> X6", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x6()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X5 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(5)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X5 -> X5", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x5()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X5 -> X5", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x5()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X -> X", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x5()
            Else
                'error
                xError(5)
            End If
        Else
            'error
            'xError(5)
        End If
    End Sub

    Public Sub x6()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                Debugger("cambio", "X6 -> XS", 0)
                'token += caracter(pos)
                xS()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X6 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(6)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X6 -> X6", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x6()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X6 -> X6", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x6()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X6 -> X6", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x6()
            Else
                xError(6)
            End If
        Else
            'error
            'xError(6)
        End If
    End Sub

    Public Sub x7()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ",") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ",", "6", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X7 -> X8", 0)
                'Aceptacion
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x8()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X7 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(7)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X7 -> X7", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x7()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X7 -> X7", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x7()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X7 -> X7", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x7()
            Else
                xError(7)
            End If
        Else
            'error
            'xError(7)
        End If
    End Sub

    Public Sub x8()
        If (pos < cadena.Length) Then
            x4()
        Else
            'error
            'xError(8)
        End If

    End Sub

    Public Sub x9()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X9 -> X10", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x10()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X9 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(9)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X9 -> X9", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x9()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X9 -> X9", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x9()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X9 -> X9", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x9()
            Else
                'error
                xError(9)
            End If
        Else
            'error
            'xError(9)
        End If
    End Sub

    Public Sub x10()
        If (pos < cadena.Length) Then
            If (Char.IsNumber(caracter(pos)) Or caracter(pos) = "(" Or caracter(pos) = ")" Or caracter(pos) = "+" Or caracter(pos) = "-" Or caracter(pos) = "*" Or caracter(pos) = "/") Then
                Debugger("cambio", "X10 -> XEC", 0)
                xEc()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X10 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(10)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X10 -> X10", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x10()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X10 -> X10", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x10()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X10 -> X10", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x10()
            Else
                xError(10)
            End If
        Else
            'xError(10)
            'error
        End If
    End Sub

    Public Sub x11()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ",") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ",", "6", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X11 -> X12", 0)
                'token += caracter(pos)
                x12()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X11 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(11)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X11 -> X11", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x11()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X11 -> X11", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x11()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X11 -> X11", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x11()
            Else
                xError(11)
            End If
        Else
            'error
            ' xError(11)
        End If
    End Sub

    Public Sub x12()
        If (pos < cadena.Length) Then
            pos += 1
            contadorColumnas += 1
            tokenTmp = ""
            x4()
        ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
            If (caracter(pos) = "/") Then
                Debugger("cambio", "X12 -> XC0", 0)
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xC0(12)
            Else
                contadorColumnas += 1
                Debugger("cambio", "X12 -> X12", 0)
                Debugger("separador", "Espacio/Tabulación", 0)
                pos += 1
                x12()
            End If
            'contadorColumnas += 1
            'Debugger("cambio", "X12 -> X12", 0)
            'Debugger("separador", "Espacio/Tabulación", 0)
            'pos += 1
            'x12()
        ElseIf (caracter(pos) = vbLf) Then
            contadorFilas += 1
            contadorColumnas = 1
            Debugger("cambio", "X12 -> X12", 0)
            Debugger("separador", "Salto de Linea", 0)
            pos += 1
            x12()
        Else
            'error
            'xError(12)
        End If
    End Sub

    Public Sub x13()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X13 -> X14", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x14()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X13 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(13)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X13 -> X13", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x13()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X13 -> X13", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x13()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X13 -> X13", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x13()
            Else
                'error
                xError(13)
            End If
        Else
            'error
            'xError(13)
        End If
    End Sub
    Public Sub x14()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                Debugger("cambio", "X14 -> XS", 0)
                'token += caracter(pos)
                xS()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X14 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(14)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X14 -> X14", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x14()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X14 -> X14", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x14()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X14 -> X14", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x14()
            Else
                xError(14)
            End If
        Else
            'error
            ' xError(14)
        End If
    End Sub

    Public Sub x15()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ",") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ",", "6", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X15 -> X16", 0)
                'token += caracter(pos)
                x16()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X15 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(15)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X15 -> X15", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x15()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X15 -> X15", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x15()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X15 -> X15", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x15()
            Else
                xError(15)
            End If
        Else
            ' xError(15)
            'error
        End If
    End Sub
    Public Sub x16()
        If (pos < cadena.Length) Then
            pos += 1
            contadorColumnas += 1
            tokenTmp = ""
            x4()
        ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
            If (caracter(pos) = "/") Then
                Debugger("cambio", "X16 -> XC0", 0)
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xC0(16)
            Else
                contadorColumnas += 1
                Debugger("cambio", "X16 -> X16", 0)
                Debugger("separador", "Espacio/Tabulación", 0)
                pos += 1
                x16()
            End If
            'contadorColumnas += 1
            'Debugger("cambio", "X16 -> X16", 0)
            'Debugger("separador", "Espacio/Tabulación", 0)
            'pos += 1
            'x16()
        ElseIf (caracter(pos) = vbLf) Then
            contadorFilas += 1
            contadorColumnas = 1
            Debugger("cambio", "X16 -> X16", 0)
            Debugger("separador", "Salto de Linea", 0)
            pos += 1
            x16()
        Else
            'error
            'xError(16)
        End If
    End Sub

    Public Sub x17()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X17 -> X18", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x18()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X17 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(17)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X17 -> X17", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x17()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X17 -> X17", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x17()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X17 -> X17", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x17()
            Else
                'error
                xError(17)
            End If
        Else
            'error
            'xError(17)
        End If
    End Sub

    Public Sub x18()
        If (pos < cadena.Length) Then
            If (caracter(pos) = "[") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "[", "3", "AperturaCorchetes")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X18 -> X19", 0)
                'token += caracter(pos)
                x19()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X18 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(18)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X18 -> X18", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x18()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X18 -> X18", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x18()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X18 -> X18", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x18()
            Else
                'error
                xError(18)
            End If
        Else
            'error
            'xError(18)
        End If
    End Sub
    Public Sub x19()
        If (pos < cadena.Length) Then

            If (caracter(pos) = "[") Then
                'token += caracter(pos)
                pos += 1
                contadorFilas += 1
                tokenTmp = ""
                x19()
            ElseIf (caracter(pos) = "{") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "{", "1", "AperturaLlaves")
                Debugger("ok", caracter(pos), 0)
                token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                tokenTmp = ""
                x20()
            ElseIf (caracter(pos) = "]") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "]", "4", "CierreCorchetes")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X28 -> X29", 0)
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x29()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X19 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(19)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X19 -> X19", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x19()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X19 -> X19", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x19()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X19 -> X19", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x19()
            Else
                xError(19)
            End If
        Else
            'error
            'xError(19)
        End If
    End Sub

    Public Sub x20()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                Debugger("cambio", "X20 -> XS", 0)
                xS()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X20 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(20)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X20 -> X20", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x20()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X20 -> X20", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x20()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X20 -> X20", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x20()
            Else
                xError(20)
            End If
        Else
            'error
            'xError(20)
        End If
    End Sub

    Public Sub x21()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X21 -> X22", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x22()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X21 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(21)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X21 -> X21", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x21()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X21 -> X21", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x21()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X21 -> X21", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x21()
            Else
                'error
                xError(21)
            End If
        Else
            'error
            'xError(21)
        End If
    End Sub

    Public Sub x22()
        If (pos < cadena.Length) Then
            If (Char.IsNumber(caracter(pos))) Then
                Debugger("cambio", "X22 -> X23", 0)
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x23()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X22 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(22)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X22 -> X22", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x22()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X22 -> X22", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x22()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X22 -> X22", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x22()
            Else
                xError(22)
            End If
        Else
            'error
            'xError(22)
        End If
    End Sub

    Public Sub x23()
        If (pos < cadena.Length) Then
            If (Char.IsNumber(caracter(pos))) Then
                Debugger("cambio", "X23 -> X23", 0)
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x23()
            ElseIf (caracter(pos) = ",") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, token, "10", "Numero")
                Debugger("ok", token, 1)
                token = ""
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - 1).ToString, ",", "6", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X23 -> X24", 0)
                'token += caracter(pos)
                x24()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X23 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(23)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "23 -> X23", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x23()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X23 -> X23", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x23()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X23 -> X23", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x23()
            Else
                xError(23)
            End If

        Else
            'error
            'xError(23)
        End If
    End Sub
    Public Sub x24()
        If (pos < cadena.Length) Then
            If (cadena(pos) = ",") Then
                pos += 1
                contadorColumnas += 1
                x20()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X24 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(24)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X24 -> X24", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x24()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X24 -> X24", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x24()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X24 -> X24", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x24()
            Else
                xError(24)
            End If
        Else
            'error
            'xError(24)
        End If

    End Sub

    Public Sub x25()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ":") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ":", "5", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X25 -> X26", 0)
                'token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                x26()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X25 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(25)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X25 -> X25", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x25()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X25 -> X25", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x25()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X25 -> X25", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x25()
            Else
                'error
                xError(25)
            End If
        Else
            'error
            ' xError(25)
        End If
    End Sub

    Public Sub x26()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                Debugger("cambio", "X26 -> XS", 0)
                'token += caracter(pos)
                xS()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X26 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(26)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X26 -> X26", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x26()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X26 -> X26", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x26()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X26 -> X26", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x26()
            Else
                'error
                xError(26)
            End If
        Else
            'error
            'xError(26)
        End If
    End Sub

    Public Sub x27()
        If (pos < cadena.Length) Then
            If (caracter(pos) = "}") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "}", "2", "CierreLlaves")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X27 -> X28", 0)
                ' token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x28()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X27 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(27)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X27 -> X27", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x27()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X27 -> X27", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x27()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X27 -> X27", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x27()
            Else
                xError(27)
            End If
        Else
            'error
            'xError(27)
        End If
    End Sub

    Public Sub x28()
        If (pos < cadena.Length) Then
            If (caracter(pos) = ",") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, ",", "6", "Separador")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X28 -> X19", 0)
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x19()
            ElseIf (caracter(pos) = "]") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "]", "4", "CierreCorchetes")
                Debugger("ok", caracter(pos), 0)
                Debugger("cambio", "X28 -> X29", 0)
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x29()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X28 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(28)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X28 -> X28", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x28()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X28 -> X28", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x28()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X28 -> X28", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x28()
            Else
                xError(28)
            End If
        Else
            'error
            'xError(28)
        End If
    End Sub

    Public Sub x29()
        If (pos < cadena.Length) Then
            If (caracter(pos) = "}") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "}", "2", "CierreLlaves")
                Debugger("ok", caracter(pos), 0)
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                x30()
            ElseIf (caracter(pos) = """") Then
                x4()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X29 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(29)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X29 -> X29", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x29()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X29 -> X29", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x29()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X29 -> X29", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x29()
            Else
                'error
                xError(29)
            End If
        Else
            'error
            ' xError(29)
        End If
    End Sub

    Public Sub x30()
        If (pos < cadena.Length) Then
            If (caracter(pos) = "}") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, "}", "2", "CierreLlaves")
                Debugger("ok", caracter(pos), 0)
                'token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                xFin()
            ElseIf (caracter(pos) = " " Or caracter(pos) = vbTab Or caracter(pos) = "/") Then
                If (caracter(pos) = "/") Then
                    Debugger("cambio", "X30 -> XC0", 0)
                    token += caracter(pos)
                    contadorColumnas += 1
                    pos += 1
                    xC0(30)
                Else
                    contadorColumnas += 1
                    Debugger("cambio", "X -> X", 0)
                    Debugger("separador", "Espacio/Tabulación", 0)
                    pos += 1
                    x30()
                End If
                'contadorColumnas += 1
                'Debugger("cambio", "X30 -> X30", 0)
                'Debugger("separador", "Espacio/Tabulación", 0)
                'pos += 1
                'x30()
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                contadorColumnas = 1
                Debugger("cambio", "X30 -> X30", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                x30()
            Else
                'x4()
                xError(30)
            End If
        Else
            'error
            ' xError(30)
        End If
    End Sub

    Public Sub xFin()
        If (pos < cadena.Length) Then
            If (Form1.ListView2.Items.Count = 0) Then
                Dim rutaDocumentosD As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                Dim generadorImagen As New Graphviz()
                Dim strTemp As String
                Dim nombreImagen As String = ""
                Dim tipoImagen As String = ""
                Dim xTrue = False, yTrue = False, colorTrue = False
                Dim X = "", Y = "", Color = ""

                If (tmpAlto = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If
                If (tmpAncho = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If

                Dim datosGraphiz As String = generadorImagen.matrizLogica(CInt(Form1.ecuacion(tmpAlto)), CInt(Form1.ecuacion(tmpAncho)))
                For i2 = 0 To Form1.ListView1.Items.Count - 1

                    If (Form1.ListView1.Items(i2).SubItems(3).Text = """y""") Then
                        'Console.WriteLine("xD")
                        yTrue = True
                    ElseIf (Form1.ListView1.Items(i2).SubItems(3).Text = """x""") Then
                        xTrue = True
                    End If
                    If (Form1.ListView1.Items(i2).SubItems(3).Text = """color""") Then
                        colorTrue = True
                    End If
                    If (Form1.ListView1.Items(i2).SubItems(4).Text = "58") Then
                        nombreImagen = Form1.ListView1.Items(i2).SubItems(3).Text.Replace("""", "")
                    End If
                    If (Form1.ListView1.Items(i2).SubItems(4).Text = "54") Then
                        tipoImagen = Form1.ListView1.Items(i2).SubItems(3).Text.Replace("""", "")
                    End If

                    If (xTrue) Then
                        'Console.WriteLine(Form1.ListView1.Items(i2).SubItems(3).Text)
                        If (IsNumeric(Form1.ListView1.Items(i2).SubItems(3).Text)) Then
                            X = Form1.ListView1.Items(i2).SubItems(3).Text
                            'Console.WriteLine(X + "----")
                            xTrue = False
                        End If
                    End If

                    If (yTrue) Then
                        If (IsNumeric(Form1.ListView1.Items(i2).SubItems(3).Text)) Then
                            Y = Form1.ListView1.Items(i2).SubItems(3).Text
                            'Console.WriteLine("----" + Y)
                            yTrue = False
                        End If
                    End If

                    If (colorTrue) Then
                        If (Form1.ListView1.Items(i2).SubItems(3).Text = """rojo""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """azul""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """blanco""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """negro""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """cafe""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """café""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """anaranjado""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """verde""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """amarillo""") Then
                            Color = Form1.ListView1.Items(i2).SubItems(3).Text
                            'colorTrue = True
                            'Console.WriteLine(Color)
                        End If
                    End If
                    If (IsNumeric(X) And IsNumeric(Y) And Color <> "") Then
                        'Console.WriteLine(X + "<---->" + Y + "---" + Color)
                        Color = Color.Replace("""", "")
                        datosGraphiz = generadorImagen.colorear(datosGraphiz, X, Y, Color)
                        X = ""
                        Y = ""
                        Color = ""
                    End If
                Next

                If (nombreImagen = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If
                If (tipoImagen = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If
                Dim rutaImagen = rutaDocumentosD + "\" + nombreImagen + "." + tipoImagen
                Dim imagenCreada As Boolean = False
                'Dim rutaImagen = rutaDocumentosD + "\hongo." + tipoImagen
                'Console.WriteLine("" + rutaImagen + "")

                Try
                    If My.Computer.FileSystem.FileExists(rutaImagen) Then
                        My.Computer.FileSystem.DeleteFile(rutaImagen)
                        Threading.Thread.Sleep(1500)
                    End If
                Catch ex As Exception
                    'MsgBox(ex)
                End Try


                generadorImagen.generarGraphviz(datosGraphiz, tipoImagen, nombreImagen)

                While (imagenCreada = False)
                    If My.Computer.FileSystem.FileExists(rutaImagen) Then
                        Form1.PictureBox1.Image = Image.FromFile(rutaImagen)
                        imagenCreada = True
                    Else
                        Threading.Thread.Sleep(2000)
                    End If
                End While
            Else
                MsgBox("El analizador ha encontrado uno o más errores por lo que no es posible generar la imagen")
            End If
            x0()
        Else
            If (Form1.ListView2.Items.Count = 0) Then
                Dim rutaDocumentosD As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                Dim generadorImagen As New Graphviz()
                Dim strTemp As String
                Dim nombreImagen As String = ""
                Dim tipoImagen As String = ""
                Dim xTrue = False, yTrue = False, colorTrue = False
                Dim X = "", Y = "", Color = ""

                If (tmpAlto = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If
                If (tmpAncho = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If

                Dim datosGraphiz As String = generadorImagen.matrizLogica(CInt(Form1.ecuacion(tmpAlto)), CInt(Form1.ecuacion(tmpAncho)))
                For i2 = 0 To Form1.ListView1.Items.Count - 1

                    If (Form1.ListView1.Items(i2).SubItems(3).Text = """y""") Then
                        'Console.WriteLine("xD")
                        yTrue = True
                    ElseIf (Form1.ListView1.Items(i2).SubItems(3).Text = """x""") Then
                        xTrue = True
                    End If
                    If (Form1.ListView1.Items(i2).SubItems(3).Text = """color""") Then
                        colorTrue = True
                    End If
                    If (Form1.ListView1.Items(i2).SubItems(4).Text = "58") Then
                        nombreImagen = Form1.ListView1.Items(i2).SubItems(3).Text.Replace("""", "")
                    End If
                    If (Form1.ListView1.Items(i2).SubItems(4).Text = "54") Then
                        tipoImagen = Form1.ListView1.Items(i2).SubItems(3).Text.Replace("""", "")
                    End If

                    If (xTrue) Then
                        'Console.WriteLine(Form1.ListView1.Items(i2).SubItems(3).Text)
                        If (IsNumeric(Form1.ListView1.Items(i2).SubItems(3).Text)) Then
                            X = Form1.ListView1.Items(i2).SubItems(3).Text
                            'Console.WriteLine(X + "----")
                            xTrue = False
                        End If
                    End If

                    If (yTrue) Then
                        If (IsNumeric(Form1.ListView1.Items(i2).SubItems(3).Text)) Then
                            Y = Form1.ListView1.Items(i2).SubItems(3).Text
                            'Console.WriteLine("----" + Y)
                            yTrue = False
                        End If
                    End If

                    If (colorTrue) Then
                        If (Form1.ListView1.Items(i2).SubItems(3).Text = """rojo""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """azul""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """blanco""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """negro""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """cafe""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """café""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """anaranjado""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """verde""" Or Form1.ListView1.Items(i2).SubItems(3).Text = """amarillo""") Then
                            Color = Form1.ListView1.Items(i2).SubItems(3).Text
                            'colorTrue = True
                            'Console.WriteLine(Color)
                        End If
                    End If
                    If (IsNumeric(X) And IsNumeric(Y) And Color <> "") Then
                        'Console.WriteLine(X + "<---->" + Y + "---" + Color)
                        Color = Color.Replace("""", "")
                        datosGraphiz = generadorImagen.colorear(datosGraphiz, X, Y, Color)
                        X = ""
                        Y = ""
                        Color = ""
                    End If
                Next

                If (nombreImagen = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If
                If (tipoImagen = "") Then
                    MsgBox("Faltan algunos parametros para poder realizar la imagen")
                    Exit Sub
                End If
                Dim rutaImagen = rutaDocumentosD + "\" + nombreImagen + "." + tipoImagen
                Dim imagenCreada As Boolean = False
                'Dim rutaImagen = rutaDocumentosD + "\hongo." + tipoImagen
                'Console.WriteLine("" + rutaImagen + "")

                Try
                    If My.Computer.FileSystem.FileExists(rutaImagen) Then
                        My.Computer.FileSystem.DeleteFile(rutaImagen)
                        Threading.Thread.Sleep(1500)
                    End If
                Catch ex As Exception
                    'MsgBox(ex)
                End Try


                generadorImagen.generarGraphviz(datosGraphiz, tipoImagen, nombreImagen)

                While (imagenCreada = False)
                    If My.Computer.FileSystem.FileExists(rutaImagen) Then
                        Form1.PictureBox1.Image = Image.FromFile(rutaImagen)
                        imagenCreada = True
                    Else
                        Threading.Thread.Sleep(2000)
                    End If
                End While
            Else
                MsgBox("El analizador ha encontrado uno o más errores por lo que no es posible generar la imagen")
            End If
            'MsgBox("analizado con exito")
        End If

    End Sub

    Public Sub xC0(estado As Integer)
        If (pos < cadena.Length) Then
            If (caracter(pos) = "/") Then
                Debugger("cambio", "XC0 -> XC1", 0)
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xC1(estado)
            Else
                xError(estado)
            End If
        Else
            'error
            xError(estado)
        End If

    End Sub

    Public Sub xC1(estado As Integer)
        If (pos < cadena.Length) Then
            If (caracter(pos) = vbLf Or pos = cadena.Length - 1) Then
                If (caracter(pos) = vbLf) Then
                Else
                    token += caracter(pos)
                End If
                func_TablaReservadas(contadorFilas, (contadorColumnas - (token.Length) + 1).ToString, token, "100", "comentario")
                Debugger("ok", token, 1)
                Debugger("cambio", "XC1 -> X0", 0)
                token = ""
                pos += 1
                estadoC(estado)
                'x0()
            Else
                Debugger("cambio", "XC1 -> XC1", 0)
                token += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xC1(estado)
            End If
        Else
            'error
            'xError()
        End If
    End Sub

    Function estadoC(estado As Integer)
        Select Case estado
            Case 0
                x0()

            Case 1
                x1()

            Case 2
                x2()

            Case 3
                x3()

            Case 4
                x4()

            Case 5
                x5()

            Case 6
                x6()

            Case 7
                x7()

            Case 8
                x8()

            Case 9
                x9()

            Case 10
                x10()

            Case 11
                x11()

            Case 12
                x12()

            Case 13
                x13()

            Case 14
                x14()

            Case 15
                x15()

            Case 16
                x16()

            Case 17
                x17()

            Case 18
                x18()

            Case 19
                x19()

            Case 20
                x20()

            Case 21
                x21()

            Case 22
                x22()

            Case 23
                x23()

            Case 24
                x24()

            Case 25
                x25()

            Case 26
                x26()

            Case 27
                x27()

            Case 28
                x28()

            Case 29
                x29()

            Case 30
                x30()

        End Select
    End Function
    Public Sub xS()
        If (pos < cadena.Length) Then
            If (caracter(pos) = """") Then
                Debugger("cambio", "XS -> XS1", 0)
                token += caracter(pos)
                tokenTmp += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xS1()
            End If
        Else
            'error
        End If
    End Sub

    Public Sub xS1()
        If (pos < cadena.Length) Then
            If (Char.IsLetter(caracter(pos))) Then
                Debugger("cambio", "XS1 -> XS2", 0)
                token += caracter(pos)
                tokenTmp += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xS2()
            Else
                xError(200)
            End If
        Else
            'error
            xError(200)
        End If
    End Sub

    Public Sub xS2()
        If (pos < cadena.Length) Then
            If (Char.IsLetter(caracter(pos))) Then
                Debugger("cambio", "XS2 -> XS2", 0)
                token += caracter(pos)
                tokenTmp += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xS2()
            ElseIf (caracter(pos) = """") Then
                Debugger("cambio", "XS2 -> XS3", 0)
                token += caracter(pos)
                tokenTmp += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xS3()
            Else
                xError(200)
            End If
        Else
            'error
            xError(200)
        End If

    End Sub

    Public Sub xS3()
        If (pos < cadena.Length) Then
            Select Case verificar_xS(tokenTmp)
                Case 0
                    'Console.WriteLine("Fila:  " + contadorFilas.ToString + "Columna: " + (contadorColumnas - (tokenTmp.Length)).ToString)
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "50", "ReservadaImagen")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X2", 0)
                    x2()
                Case 1
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "51", "ReservadaNombre")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X5", 0)
                    x5()
                Case 2
                    If (tokenTmp = """alto""") Then
                        func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "52", "ReservadaAlto")
                        anterior_alto = True
                    Else
                        func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "53", "ReservadaAncho")
                        anterior_ancho = True
                    End If

                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X9", 0)
                    x9()
                Case 4
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "49", "ReservadaFormato")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X13", 0)
                    x13()
                Case 5
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "54", "TipoImagen")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X15", 0)
                    x15()
                Case 6
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "55", "ReservadaColores")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X17", 0)
                    x17()
                Case 7, 8
                    If (tokenTmp = """x""") Then
                        func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "56", "CoordenadaX")
                    Else
                        func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "57", "CoordenadaY")
                    End If
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X21", 0)
                    x21()
                Case 9
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "58", "TipoNombre")
                    Debugger("ok", tokenTmp, 1)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X7", 0)
                    x7()
                Case 10
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "59", "ReservadaColor")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X7", 0)
                    x25()
                Case 11
                    func_TablaReservadas(contadorFilas.ToString, (contadorColumnas - (token.Length)).ToString, tokenTmp, "60", "ColorPixel")
                    Debugger("ok", tokenTmp, 0)
                    tokenTmp = ""
                    Debugger("cambio", "XS3 -> X7", 0)
                    x27()
            End Select
        Else
            'error
            'xError(200)
        End If
    End Sub

    Function verificar_xS(token)
        If (token = """imagen""") Then
            Return 0
        ElseIf (token = """nombre""") Then
            Return 1
        ElseIf (token = """alto""" Or token = """ancho""") Then
            Return 2
            'ElseIf (token = """ancho""") Then
            ''Return 3
        ElseIf (token = """formato""") Then
            Return 4
        ElseIf (token = """jpg""" Or token = """png""" Or token = """bmp""") Then
            Return 5
        ElseIf (token = """colores""") Then
            Return 6
        ElseIf (token = """x""") Then
            Return 7
        ElseIf (token = """y""") Then
            Return 8
        ElseIf (token = """color""") Then
            Return 10
        ElseIf (token = """rojo""" Or token = """azul""" Or token = """blanco""" Or token = """negro""" Or token = """cafe""" Or token = """café""" Or token = """anaranjado""" Or token = """amarillo""" Or token = """verde""") Then
            Return 11
        Else
            Return 9
        End If
    End Function

    Public Sub xEc()
        If (pos < cadena.Length) Then
            If (Char.IsNumber(caracter(pos)) Or caracter(pos) = "(" Or caracter(pos) = ")" Or caracter(pos) = "+" Or caracter(pos) = "-" Or caracter(pos) = "*" Or caracter(pos) = "/") Then
                Debugger("cambio", "XEC -> XEC", 0)
                token += caracter(pos)
                tokenTmp += caracter(pos)
                contadorColumnas += 1
                pos += 1
                xEc()
            ElseIf (caracter(pos) = ",") Then
                func_TablaReservadas(contadorFilas.ToString, (contadorColumnas).ToString, tokenTmp, "70", "EcuacionNumerica")
                If (anterior_alto = True) Then
                    tmpAlto = tokenTmp
                    anterior_alto = False
                ElseIf (anterior_ancho = True) Then
                    tmpAncho = tokenTmp
                    anterior_ancho = False
                End If
                Debugger("ok", tokenTmp, 1)
                Debugger("cambio", "XEC -> X11", 0)
                'contadorColumnas += 1
                x11()
            Else
                xError(300)
            End If
        Else
            'error
            xError(300)
        End If
    End Sub
    Public Sub xError(estado As Integer)
        'Console.WriteLine("Entro a error" + token + "posicion: " + pos.ToString)
        Console.WriteLine(estado)
        If (pos < cadena.Length) Then
            If (caracter(pos) = " " Or caracter(pos) = vbTab) Then
                contadorColumnas += 1
                Debugger("cambio", "XERROR -> XERROR", 0)
                Debugger("separador", "Espacio/Tabulación", 0)
                pos += 1
                xError(estado)
            ElseIf (caracter(pos) = vbLf) Then
                contadorFilas += 1
                'contadorColumnas = 1
                Debugger("cambio", "XERROR -> XERROR", 0)
                Debugger("separador", "Salto de Linea", 0)
                pos += 1
                xError(estado)
            ElseIf (caracter(pos) = "{" And estado = 0) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x0()
            ElseIf (caracter(pos) = """" And estado = 1) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x1()
            ElseIf (caracter(pos) = ":" And estado = 2) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x2()
            ElseIf (caracter(pos) = "{" And estado = 3) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x3()
            ElseIf (caracter(pos) = """" And estado = 4) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x4()
            ElseIf (caracter(pos) = ":" And estado = 5) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x5()
            ElseIf (caracter(pos) = """" And estado = 6) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x6()
            ElseIf (caracter(pos) = "," And estado = 7) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x7()
            ElseIf (caracter(pos) = ":" And estado = 9) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x9()
            ElseIf (Char.IsNumber(caracter(pos)) And estado = 10 Or caracter(pos) = "(" And estado = 10 Or caracter(pos) = ")" And estado = 10 Or caracter(pos) = "+" And estado = 10 Or caracter(pos) = "-" And estado = 10 Or caracter(pos) = "*" And estado = 10 Or caracter(pos) = "/" And estado = 10) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x10()
            ElseIf (caracter(pos) = "," And estado = 11) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x11()
            ElseIf (caracter(pos) = ":" And estado = 13) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x13()
            ElseIf (caracter(pos) = """" And estado = 14) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x14()
            ElseIf (caracter(pos) = "," And estado = 15) Then
                '  Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x15()
            ElseIf (caracter(pos) = ":" And estado = 17) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x17()
            ElseIf (caracter(pos) = "[" And estado = 18) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x18()
            ElseIf (caracter(pos) = "[" And estado = 19 Or caracter(pos) = "{" And estado = 19) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x19()
            ElseIf (caracter(pos) = """" And estado = 20) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x20()
            ElseIf (caracter(pos) = ":" And estado = 21) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x21()
            ElseIf (Char.IsNumber(caracter(pos)) And estado = 22) Then
                '  Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x22()
            ElseIf (Char.IsNumber(caracter(pos)) And estado = 23 Or caracter(pos) = "," And estado = 23) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x23()
            ElseIf (caracter(pos) = ":" And estado = 25) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x25()
            ElseIf (caracter(pos) = """" And estado = 26) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x26()
            ElseIf (caracter(pos) = "}" And estado = 27) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x27()
            ElseIf (caracter(pos) = "," And estado = 28 Or caracter(pos) = "]" And estado = 28) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x28()
            ElseIf (caracter(pos) = "}" And estado = 29 Or caracter(pos) = """" And estado = 29) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                x29()
            ElseIf (caracter(pos) = """" And estado = 200) Then
                ' Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                tokenTmp = ""
                xS()
            ElseIf (Char.IsNumber(caracter(pos)) Or caracter(pos) = "(" Or caracter(pos) = ")" Or caracter(pos) = "+" Or caracter(pos) = "-" Or caracter(pos) = "*" Or caracter(pos) = "/" And estado = 300) Then
                'Console.WriteLine(caracter(pos) + "Error - " + token)
                Debugger("Error", token, 0)
                func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
                'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
                token = ""
                tokenTmp = ""
                xEc()
            Else
                Debugger("cambio", "XERROR -> XERROR", 0)
                token += caracter(pos)
                pos += 1
                contadorColumnas += 1
                xError(estado)
            End If
        Else
            'func_TablaErrores(filaContador, contadorColumnas, palabraTemporal, "Desconocido")
            Debugger("Error", token, 0)
            func_TablaErrores(contadorFilas - 1, (contadorColumnas - (token.Length)).ToString, token, "Desconocido")
            'Debugger("cambio", "XERROR -> XERROR", 0)
            'Console.WriteLine("Error - " + token)
        End If
    End Sub
    Function Debugger(Opcion, token_aceptadocambioEstado, ref)
        If (Opcion = "ok") Then
            If (verificarToken(token_aceptadocambioEstado, ref)) Then
                Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.Blue
                Form1.txtboxGroupBoxDebugger_Debugger.SelectionFont = New Font(Form1.txtboxGroupBoxDebugger_Debugger.Font, FontStyle.Bold)
                Form1.txtboxGroupBoxDebugger_Debugger.AppendText("Token Aceptado -> ")
                Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.Green
                Form1.txtboxGroupBoxDebugger_Debugger.AppendText(token_aceptadocambioEstado + vbNewLine)
                Form1.txtboxGroupBoxDebugger_Debugger.ScrollToCaret()
                token = ""
            Else
                Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.Red
                Form1.txtboxGroupBoxDebugger_Debugger.SelectionFont = New Font(Form1.txtboxGroupBoxDebugger_Debugger.Font, FontStyle.Bold)
                Form1.txtboxGroupBoxDebugger_Debugger.AppendText("Token NO Reconocido -> ")
                Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.Green
                Form1.txtboxGroupBoxDebugger_Debugger.AppendText(token_aceptadocambioEstado + vbNewLine)
                Form1.txtboxGroupBoxDebugger_Debugger.ScrollToCaret()
            End If
        ElseIf (Opcion = "Error") Then
            Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.Red
            Form1.txtboxGroupBoxDebugger_Debugger.SelectionFont = New Font(Form1.txtboxGroupBoxDebugger_Debugger.Font, FontStyle.Bold)
            Form1.txtboxGroupBoxDebugger_Debugger.AppendText("Token NO Reconocido -> ")
            Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.Green
            Form1.txtboxGroupBoxDebugger_Debugger.AppendText(token_aceptadocambioEstado + vbNewLine)
            Form1.txtboxGroupBoxDebugger_Debugger.ScrollToCaret()
        ElseIf (Opcion = "cambio") Then
            Form1.txtboxGroupBoxDebugger_Debugger.SelectionFont = New Font(Form1.txtboxGroupBoxDebugger_Debugger.Font, FontStyle.Bold)
            Form1.txtboxGroupBoxDebugger_Debugger.AppendText("Cambio Estado: " + token_aceptadocambioEstado + vbNewLine)
            Form1.txtboxGroupBoxDebugger_Debugger.ScrollToCaret()
        ElseIf (Opcion = "separador") Then
            Form1.txtboxGroupBoxDebugger_Debugger.SelectionFont = New Font(Form1.txtboxGroupBoxDebugger_Debugger.Font, FontStyle.Bold)
            Form1.txtboxGroupBoxDebugger_Debugger.SelectionColor = Color.BlueViolet
            Form1.txtboxGroupBoxDebugger_Debugger.AppendText("Omitido: " + token_aceptadocambioEstado + vbNewLine)
            Form1.txtboxGroupBoxDebugger_Debugger.ScrollToCaret()
        End If
    End Function

    Function verificarToken(token As String, ref As Integer)
        If (ref = 0) Then
            If (token = "{" Or token = "}" Or token = "[" Or token = "]" Or token = ":" Or token = ",") Then
                Return True
            ElseIf (token = """imagen""") Then
                Return True
            ElseIf (token = """nombre""") Then
                Return True
            ElseIf (token = """alto""") Then
                Return True
            ElseIf (token = """ancho""") Then
                Return True
            ElseIf (token = """formato""") Then
                Return True
            ElseIf (token = """jpg""" Or token = """png""" Or token = """bmp""") Then
                Return True
            ElseIf (token = """colores""") Then
                Return True
            ElseIf (token = """x""") Then
                Return True
            ElseIf (token = """y""") Then
                Return True
            ElseIf (token = """color""") Then
                Return True
            ElseIf (token = """rojo""" Or token = """azul""" Or token = """blanco""" Or token = """negro""" Or token = """cafe""" Or token = """café""" Or token = """anaranjado""" Or token = """amarillo""" Or token = """verde""") Then
                Return True
            Else
                Return False
            End If
        ElseIf (ref = 1) Then
            Return True
        End If
    End Function

    Public Function func_TablaReservadas(Fila As String, Columna As String, Lexema As String, IDToken As String, Token As String)
        Dim tablaLexemas As ListViewItem
        Form1.ListView1.BeginUpdate()
        tablaLexemas = Form1.ListView1.Items.Add(Form1.contadorLex)
        tablaLexemas.SubItems.Add(Fila)
        tablaLexemas.SubItems.Add(Columna)
        tablaLexemas.SubItems.Add(Lexema)
        tablaLexemas.SubItems.Add(IDToken)
        tablaLexemas.SubItems.Add(Token)
        Form1.contadorLex += 1
        Form1.ListView1.Update()
        Form1.ListView1.EndUpdate()
    End Function

    Public Function func_TablaErrores(Fila As String, Columna As String, errorE As String, Descripcion As String)
        Dim tablaLexemas As ListViewItem
        Form1.ListView2.BeginUpdate()
        tablaLexemas = Form1.ListView2.Items.Add(Form1.contadorLexErrores)
        tablaLexemas.SubItems.Add(Fila)
        tablaLexemas.SubItems.Add(Columna)
        tablaLexemas.SubItems.Add(errorE)
        tablaLexemas.SubItems.Add(Descripcion)
        Form1.contadorLexErrores += 1
        Form1.ListView2.Update()
        Form1.ListView2.EndUpdate()
    End Function
End Class
