﻿Public Class Graphviz
    Public Function matrizLogica(X As Integer, Y As Integer)
        Dim resultado As String
        resultado = "
                    digraph structs {
                        node [shape=plaintext] struct1 [label=<
                            <TABLE BORDER='0' CELLBORDER='1' CELLSPACING='0'>
                    "
        For i = 0 To Y - 1
            resultado += "<TR>" + vbNewLine
            For i2 = 0 To X - 1
                resultado += "<TD BGCOLOR='white'>" + i2.ToString + "," + i.ToString + "</TD>" + vbNewLine
                'Console.WriteLine("X" + i.ToString + i2.ToString)
            Next
            resultado += "</TR>" + vbNewLine
        Next
        resultado += "
                            </TABLE>
                        >];
                       }
                    "
        Return resultado
    End Function

    Public Function colorear(cadena As String, X As String, Y As String, color As String)
        If (color = "rojo") Then
            color = "red"
        ElseIf (color = "amarillo") Then
            color = "yellow"
        ElseIf (color = "blanco") Then
            color = "white"
        ElseIf (color = "negro") Then
            color = "black"
        ElseIf (color = "café" Or color = "cafe") Then
            color = "brown"
        ElseIf (color = "anaranjado") Then
            color = "orange"
        ElseIf (color = "azul") Then
            color = "blue"
        ElseIf (color = "verde") Then
            color = "green"
        End If
        cadena = cadena.Replace("<TD BGCOLOR='white'>" + X + "," + Y + "</TD>", "<TD BGCOLOR='" + color + "'>" + X + "," + Y + "</TD>")
        Return cadena
    End Function

    'Generación de la imagen de bits
    Public Sub generarGraphviz(cadena As String, tipoImagen As String, nombre As String)
        If (nombre = "") Then
            nombre = "grafo"
        End If
        If (tipoImagen = "") Then
            tipoImagen = "jpg"
        End If
        Dim rutaDocumentosD As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

        'Try
        '    If My.Computer.FileSystem.FileExists(rutaDocumentosD + "\" + nombre + "." + tipoImagen) Then
        '        My.Computer.FileSystem.DeleteFile(rutaDocumentosD + "\" + nombre + "." + tipoImagen)
        '        'Threading.Thread.Sleep(1500)
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex)
        'End Try


        Dim utf8WithoutBom As New System.Text.UTF8Encoding(False)
        My.Computer.FileSystem.WriteAllText(rutaDocumentosD + " \infoGrafo.dot", cadena, False, utf8WithoutBom)

        'Generamos un nuevo proceso
        Dim grafo As New Process

        'Ruta del compilador de graphviz
        grafo.StartInfo.FileName = "C:\graphviz\bin\dot.exe"

        'Parametros del archivo
        grafo.StartInfo.Arguments = "-Tpng -o """ + rutaDocumentosD + "\" + nombre + "." + tipoImagen + """ " + """" + rutaDocumentosD + "\infoGrafo.dot"""
        grafo.Start()
    End Sub
End Class
