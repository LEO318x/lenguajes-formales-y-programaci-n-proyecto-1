﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btn_CerrarPestaña = New System.Windows.Forms.Button()
        Me.btn_NuevaPestaña = New System.Windows.Forms.Button()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox_VistaPrevia = New Proyecto_1__LFP_1S2016_.BonfireGroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblGroupBoxVistaPrevia_Titulo = New Proyecto_1__LFP_1S2016_.BonfireLabel()
        Me.GroupBox_Debugger = New Proyecto_1__LFP_1S2016_.BonfireGroupBox()
        Me.txtboxGroupBoxDebugger_Debugger = New System.Windows.Forms.RichTextBox()
        Me.lblGroupBoxDebugger_Titulo = New Proyecto_1__LFP_1S2016_.BonfireLabel()
        Me.GroupBox_Funciones = New Proyecto_1__LFP_1S2016_.BonfireGroupBox()
        Me.btnGroupBoxFunciones_GenerarArchivos = New Proyecto_1__LFP_1S2016_.BonfireButton()
        Me.btnGroupBoxFunciones_Analizar = New Proyecto_1__LFP_1S2016_.BonfireButton()
        Me.btnGroupBoxFunciones_Guardar = New Proyecto_1__LFP_1S2016_.BonfireButton()
        Me.lblGroupBoxFunciones_Titulo = New Proyecto_1__LFP_1S2016_.BonfireLabel()
        Me.btnGroupBoxFunciones_Abrir = New Proyecto_1__LFP_1S2016_.BonfireButton()
        Me.TabControl_Pestañas = New Proyecto_1__LFP_1S2016_.BonfireTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtBox_Pestaña = New System.Windows.Forms.RichTextBox()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox_VistaPrevia.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_Debugger.SuspendLayout()
        Me.GroupBox_Funciones.SuspendLayout()
        Me.TabControl_Pestañas.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(699, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManualToolStripMenuItem, Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "&Ayuda"
        '
        'ManualToolStripMenuItem
        '
        Me.ManualToolStripMenuItem.Name = "ManualToolStripMenuItem"
        Me.ManualToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ManualToolStripMenuItem.Text = "Manual"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AcercaDeToolStripMenuItem.Text = "Acerca de..."
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6})
        Me.ListView1.Location = New System.Drawing.Point(689, 424)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(364, 119)
        Me.ListView1.TabIndex = 9
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        Me.ListView1.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "#"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Fila"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Columna"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Lexema"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ID Token"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Token"
        '
        'btn_CerrarPestaña
        '
        Me.btn_CerrarPestaña.Image = CType(resources.GetObject("btn_CerrarPestaña.Image"), System.Drawing.Image)
        Me.btn_CerrarPestaña.Location = New System.Drawing.Point(562, 27)
        Me.btn_CerrarPestaña.Name = "btn_CerrarPestaña"
        Me.btn_CerrarPestaña.Size = New System.Drawing.Size(48, 42)
        Me.btn_CerrarPestaña.TabIndex = 11
        Me.btn_CerrarPestaña.UseVisualStyleBackColor = True
        '
        'btn_NuevaPestaña
        '
        Me.btn_NuevaPestaña.Image = CType(resources.GetObject("btn_NuevaPestaña.Image"), System.Drawing.Image)
        Me.btn_NuevaPestaña.Location = New System.Drawing.Point(487, 27)
        Me.btn_NuevaPestaña.Name = "btn_NuevaPestaña"
        Me.btn_NuevaPestaña.Size = New System.Drawing.Size(47, 42)
        Me.btn_NuevaPestaña.TabIndex = 12
        Me.btn_NuevaPestaña.UseVisualStyleBackColor = True
        '
        'ListView2
        '
        Me.ListView2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11})
        Me.ListView2.Location = New System.Drawing.Point(694, 314)
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(314, 97)
        Me.ListView2.TabIndex = 13
        Me.ListView2.UseCompatibleStateImageBehavior = False
        Me.ListView2.View = System.Windows.Forms.View.Details
        Me.ListView2.Visible = False
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "#"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Fila"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Columna"
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Carácter"
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Descripción"
        Me.ColumnHeader11.Width = 66
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(560, 577)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(77, 20)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Bitacora"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(472, 577)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(77, 20)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = "HTML"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox_VistaPrevia
        '
        Me.GroupBox_VistaPrevia.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.GroupBox_VistaPrevia.Controls.Add(Me.PictureBox1)
        Me.GroupBox_VistaPrevia.Controls.Add(Me.lblGroupBoxVistaPrevia_Titulo)
        Me.GroupBox_VistaPrevia.Location = New System.Drawing.Point(425, 78)
        Me.GroupBox_VistaPrevia.Name = "GroupBox_VistaPrevia"
        Me.GroupBox_VistaPrevia.Size = New System.Drawing.Size(257, 213)
        Me.GroupBox_VistaPrevia.TabIndex = 6
        Me.GroupBox_VistaPrevia.Text = "Vista Previa"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(4, 21)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(250, 189)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'lblGroupBoxVistaPrevia_Titulo
        '
        Me.lblGroupBoxVistaPrevia_Titulo.AutoSize = True
        Me.lblGroupBoxVistaPrevia_Titulo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.lblGroupBoxVistaPrevia_Titulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.lblGroupBoxVistaPrevia_Titulo.Location = New System.Drawing.Point(93, 5)
        Me.lblGroupBoxVistaPrevia_Titulo.Name = "lblGroupBoxVistaPrevia_Titulo"
        Me.lblGroupBoxVistaPrevia_Titulo.Size = New System.Drawing.Size(75, 13)
        Me.lblGroupBoxVistaPrevia_Titulo.TabIndex = 0
        Me.lblGroupBoxVistaPrevia_Titulo.Text = "Vista Previa"
        '
        'GroupBox_Debugger
        '
        Me.GroupBox_Debugger.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.GroupBox_Debugger.Controls.Add(Me.txtboxGroupBoxDebugger_Debugger)
        Me.GroupBox_Debugger.Controls.Add(Me.lblGroupBoxDebugger_Titulo)
        Me.GroupBox_Debugger.Location = New System.Drawing.Point(425, 420)
        Me.GroupBox_Debugger.Name = "GroupBox_Debugger"
        Me.GroupBox_Debugger.Size = New System.Drawing.Size(257, 154)
        Me.GroupBox_Debugger.TabIndex = 5
        Me.GroupBox_Debugger.Text = "Debugger"
        '
        'txtboxGroupBoxDebugger_Debugger
        '
        Me.txtboxGroupBoxDebugger_Debugger.Location = New System.Drawing.Point(4, 21)
        Me.txtboxGroupBoxDebugger_Debugger.Name = "txtboxGroupBoxDebugger_Debugger"
        Me.txtboxGroupBoxDebugger_Debugger.ReadOnly = True
        Me.txtboxGroupBoxDebugger_Debugger.Size = New System.Drawing.Size(250, 130)
        Me.txtboxGroupBoxDebugger_Debugger.TabIndex = 7
        Me.txtboxGroupBoxDebugger_Debugger.Text = ""
        '
        'lblGroupBoxDebugger_Titulo
        '
        Me.lblGroupBoxDebugger_Titulo.AutoSize = True
        Me.lblGroupBoxDebugger_Titulo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.lblGroupBoxDebugger_Titulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.lblGroupBoxDebugger_Titulo.Location = New System.Drawing.Point(106, 4)
        Me.lblGroupBoxDebugger_Titulo.Name = "lblGroupBoxDebugger_Titulo"
        Me.lblGroupBoxDebugger_Titulo.Size = New System.Drawing.Size(63, 13)
        Me.lblGroupBoxDebugger_Titulo.TabIndex = 0
        Me.lblGroupBoxDebugger_Titulo.Text = "Debugger"
        '
        'GroupBox_Funciones
        '
        Me.GroupBox_Funciones.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.GroupBox_Funciones.Controls.Add(Me.btnGroupBoxFunciones_GenerarArchivos)
        Me.GroupBox_Funciones.Controls.Add(Me.btnGroupBoxFunciones_Analizar)
        Me.GroupBox_Funciones.Controls.Add(Me.btnGroupBoxFunciones_Guardar)
        Me.GroupBox_Funciones.Controls.Add(Me.lblGroupBoxFunciones_Titulo)
        Me.GroupBox_Funciones.Controls.Add(Me.btnGroupBoxFunciones_Abrir)
        Me.GroupBox_Funciones.Location = New System.Drawing.Point(418, 297)
        Me.GroupBox_Funciones.Name = "GroupBox_Funciones"
        Me.GroupBox_Funciones.Size = New System.Drawing.Size(272, 117)
        Me.GroupBox_Funciones.TabIndex = 4
        Me.GroupBox_Funciones.Text = "Funciones"
        '
        'btnGroupBoxFunciones_GenerarArchivos
        '
        Me.btnGroupBoxFunciones_GenerarArchivos.ButtonStyle = Proyecto_1__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnGroupBoxFunciones_GenerarArchivos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGroupBoxFunciones_GenerarArchivos.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGroupBoxFunciones_GenerarArchivos.Image = CType(resources.GetObject("btnGroupBoxFunciones_GenerarArchivos.Image"), System.Drawing.Image)
        Me.btnGroupBoxFunciones_GenerarArchivos.Location = New System.Drawing.Point(142, 66)
        Me.btnGroupBoxFunciones_GenerarArchivos.Name = "btnGroupBoxFunciones_GenerarArchivos"
        Me.btnGroupBoxFunciones_GenerarArchivos.RoundedCorners = True
        Me.btnGroupBoxFunciones_GenerarArchivos.Size = New System.Drawing.Size(122, 36)
        Me.btnGroupBoxFunciones_GenerarArchivos.TabIndex = 4
        Me.btnGroupBoxFunciones_GenerarArchivos.Text = "Generar Arch."
        '
        'btnGroupBoxFunciones_Analizar
        '
        Me.btnGroupBoxFunciones_Analizar.ButtonStyle = Proyecto_1__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnGroupBoxFunciones_Analizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGroupBoxFunciones_Analizar.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnGroupBoxFunciones_Analizar.Image = CType(resources.GetObject("btnGroupBoxFunciones_Analizar.Image"), System.Drawing.Image)
        Me.btnGroupBoxFunciones_Analizar.Location = New System.Drawing.Point(14, 67)
        Me.btnGroupBoxFunciones_Analizar.Name = "btnGroupBoxFunciones_Analizar"
        Me.btnGroupBoxFunciones_Analizar.RoundedCorners = True
        Me.btnGroupBoxFunciones_Analizar.Size = New System.Drawing.Size(122, 35)
        Me.btnGroupBoxFunciones_Analizar.TabIndex = 3
        Me.btnGroupBoxFunciones_Analizar.Text = "Analizar"
        '
        'btnGroupBoxFunciones_Guardar
        '
        Me.btnGroupBoxFunciones_Guardar.ButtonStyle = Proyecto_1__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnGroupBoxFunciones_Guardar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGroupBoxFunciones_Guardar.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnGroupBoxFunciones_Guardar.Image = CType(resources.GetObject("btnGroupBoxFunciones_Guardar.Image"), System.Drawing.Image)
        Me.btnGroupBoxFunciones_Guardar.Location = New System.Drawing.Point(142, 25)
        Me.btnGroupBoxFunciones_Guardar.Name = "btnGroupBoxFunciones_Guardar"
        Me.btnGroupBoxFunciones_Guardar.RoundedCorners = True
        Me.btnGroupBoxFunciones_Guardar.Size = New System.Drawing.Size(122, 35)
        Me.btnGroupBoxFunciones_Guardar.TabIndex = 2
        Me.btnGroupBoxFunciones_Guardar.Text = "Guardar"
        '
        'lblGroupBoxFunciones_Titulo
        '
        Me.lblGroupBoxFunciones_Titulo.AutoSize = True
        Me.lblGroupBoxFunciones_Titulo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.lblGroupBoxFunciones_Titulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.lblGroupBoxFunciones_Titulo.Location = New System.Drawing.Point(110, 6)
        Me.lblGroupBoxFunciones_Titulo.Name = "lblGroupBoxFunciones_Titulo"
        Me.lblGroupBoxFunciones_Titulo.Size = New System.Drawing.Size(63, 13)
        Me.lblGroupBoxFunciones_Titulo.TabIndex = 1
        Me.lblGroupBoxFunciones_Titulo.Text = "Funciones"
        '
        'btnGroupBoxFunciones_Abrir
        '
        Me.btnGroupBoxFunciones_Abrir.ButtonStyle = Proyecto_1__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnGroupBoxFunciones_Abrir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGroupBoxFunciones_Abrir.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnGroupBoxFunciones_Abrir.Image = CType(resources.GetObject("btnGroupBoxFunciones_Abrir.Image"), System.Drawing.Image)
        Me.btnGroupBoxFunciones_Abrir.Location = New System.Drawing.Point(14, 25)
        Me.btnGroupBoxFunciones_Abrir.Name = "btnGroupBoxFunciones_Abrir"
        Me.btnGroupBoxFunciones_Abrir.RoundedCorners = True
        Me.btnGroupBoxFunciones_Abrir.Size = New System.Drawing.Size(122, 36)
        Me.btnGroupBoxFunciones_Abrir.TabIndex = 0
        Me.btnGroupBoxFunciones_Abrir.Text = "Abrir"
        '
        'TabControl_Pestañas
        '
        Me.TabControl_Pestañas.Controls.Add(Me.TabPage1)
        Me.TabControl_Pestañas.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.TabControl_Pestañas.ItemSize = New System.Drawing.Size(0, 30)
        Me.TabControl_Pestañas.Location = New System.Drawing.Point(12, 38)
        Me.TabControl_Pestañas.Name = "TabControl_Pestañas"
        Me.TabControl_Pestañas.SelectedIndex = 0
        Me.TabControl_Pestañas.Size = New System.Drawing.Size(374, 559)
        Me.TabControl_Pestañas.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.txtBox_Pestaña)
        Me.TabPage1.Location = New System.Drawing.Point(4, 34)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(366, 521)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Pestaña 1"
        '
        'txtBox_Pestaña
        '
        Me.txtBox_Pestaña.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBox_Pestaña.Location = New System.Drawing.Point(6, 6)
        Me.txtBox_Pestaña.Name = "txtBox_Pestaña"
        Me.txtBox_Pestaña.Size = New System.Drawing.Size(354, 509)
        Me.txtBox_Pestaña.TabIndex = 0
        Me.txtBox_Pestaña.Text = ""
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(699, 612)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListView2)
        Me.Controls.Add(Me.btn_NuevaPestaña)
        Me.Controls.Add(Me.btn_CerrarPestaña)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.GroupBox_VistaPrevia)
        Me.Controls.Add(Me.GroupBox_Debugger)
        Me.Controls.Add(Me.GroupBox_Funciones)
        Me.Controls.Add(Me.TabControl_Pestañas)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Proyecto #1 - LFP_1S2016 | Mike Molina 2012-12535"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox_VistaPrevia.ResumeLayout(False)
        Me.GroupBox_VistaPrevia.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_Debugger.ResumeLayout(False)
        Me.GroupBox_Debugger.PerformLayout()
        Me.GroupBox_Funciones.ResumeLayout(False)
        Me.GroupBox_Funciones.PerformLayout()
        Me.TabControl_Pestañas.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ManualToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabControl_Pestañas As BonfireTabControl
    Friend WithEvents GroupBox_Funciones As BonfireGroupBox
    Friend WithEvents btnGroupBoxFunciones_GenerarArchivos As BonfireButton
    Friend WithEvents btnGroupBoxFunciones_Analizar As BonfireButton
    Friend WithEvents btnGroupBoxFunciones_Guardar As BonfireButton
    Friend WithEvents lblGroupBoxFunciones_Titulo As BonfireLabel
    Friend WithEvents btnGroupBoxFunciones_Abrir As BonfireButton
    Friend WithEvents GroupBox_Debugger As BonfireGroupBox
    Friend WithEvents lblGroupBoxDebugger_Titulo As BonfireLabel
    Friend WithEvents GroupBox_VistaPrevia As BonfireGroupBox
    Friend WithEvents lblGroupBoxVistaPrevia_Titulo As BonfireLabel
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents txtboxGroupBoxDebugger_Debugger As RichTextBox
    Friend WithEvents ListView1 As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents txtBox_Pestaña As RichTextBox
    Friend WithEvents btn_CerrarPestaña As Button
    Friend WithEvents btn_NuevaPestaña As Button
    Friend WithEvents ListView2 As ListView
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents ColumnHeader10 As ColumnHeader
    Friend WithEvents ColumnHeader11 As ColumnHeader
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class
