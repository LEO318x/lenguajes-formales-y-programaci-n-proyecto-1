﻿Public Class Evaluar

    Dim signo As Integer
    Function Operar(ByVal ecuacion As String) As String
        Dim Abierto As Integer, Cerrado As Integer
        Dim PosAbierto As Integer, PosCerrado As Integer
        Dim ExpresionParentesis As String
        'Dim T As String

        'Revisamos si hay parentesis abiertos 
        For i = 1 To Len(ecuacion) 'tamaño de la cadena (ecuacion)
            If Mid(ecuacion, i, 1) = "(" Then 'Extrae los )
                Abierto += 1 'Siempre que encontremos un ( le sumamos 1 a "Abierto"
            End If
        Next i

        'Revisamos si hay parentesis cerrados
        For i = 1 To Len(ecuacion) 'tamaño de la cadena (ecuacion)
            If Mid(ecuacion, i, 1) = ")" Then 'Extrae los )
                Cerrado += 1 'Siempre que encontremos un ) le sumamos 1 a "Cerrado"
            End If
        Next i

        'Si los parentesis no coinciden con el numero de abiertos y cerrados, desplegamos error
        If Abierto <> Cerrado Then
            MsgBox("Ooops problamente tengas algun parentesis de más, la imagen no sera generada")
            Exit Function
        ElseIf (Abierto = 0 And Cerrado = 0) Then
            'Ya que no encontramos parentesis realizamos la operacion directamente
            Operar = OperarExpresion(ecuacion)
            Exit Function
        End If

        While Abierto > 0
            'Buscamos el ultimo parentesis ( 
            PosAbierto = InStrRev(ecuacion, "(")

            'Buscamos el parentesis que ) 
            PosCerrado = InStr(Mid(ecuacion, PosAbierto + 1), ")")

            'Operamos la expresion que se encuentra contenida en todo el parentesis
            ExpresionParentesis = OperarExpresion(Mid(ecuacion, PosAbierto + 1, PosCerrado - 1))

            'Eliminamos parentesis y reemplazamos la expresion por el resultado
            ecuacion = Left(ecuacion, PosAbierto - 1) & ExpresionParentesis & Mid(ecuacion, PosAbierto + PosCerrado + 1)
            Abierto -= 1
        End While
        'Ya que terminamos de valuar todos los parentesis, operamos por si existiera alguna otra cantidad fuera del parentesis
        Operar = OperarExpresion(ecuacion)

    End Function

    Function OperarExpresion(ecuacion As String) As String
        Dim CodigoAsc As Integer, contador As Integer
        Dim VF As Boolean
        Dim numero1 As Integer, numero2 As Integer, Signo As Integer
        Dim Resultado As String, Fin As Boolean

        'Quitamos los espacios  
        ecuacion = Replace(ecuacion, " ", "")

        While Not Fin
            For i = 1 To Len(ecuacion)
                CodigoAsc = Asc(Mid(ecuacion, i, 1)) 'Devolvemos el caracter en codigo ascii
                If CodigoAsc < 48 And CodigoAsc <> 46 Or i = Len(ecuacion) Then
                    If VF Then 'Operamos  
                        If i = Len(ecuacion) Then
                            numero2 = Val(Mid(ecuacion, contador)) 'Devuelve valor numerico de la cadena
                        Else
                            numero2 = Val(Mid(ecuacion, contador, i - contador))
                        End If
                        Resultado = Str(OperacionesBasicas(numero1, numero2, Signo)) 'Resultado de la operacion como un string
                        If i = Len(ecuacion) Then
                            Fin = True
                        Else
                            ecuacion = Trim(Resultado & Mid(ecuacion, i)) 'Quitamos todos los espacios al principio y al final de la cadena
                            VF = False
                        End If
                        Exit For
                    Else 'Separamos la cifra 
                        numero1 = Val(Left(ecuacion, i - 1))
                        Signo = CodigoAsc
                        contador = i + 1
                        VF = True
                    End If
                End If
            Next i
        End While
        'Reemplazamos la operación por el resultado dado 
        OperarExpresion = Trim(Resultado) 'Por si hubiera espacios los quitamos de nuevo
    End Function

    Function OperacionesBasicas(n1 As Integer, n2 As Integer, Signo As Integer) As Integer
        If (Signo = 43) Then 'Suma
            OperacionesBasicas = n1 + n2
        ElseIf (Signo = 45) Then 'Resta
            OperacionesBasicas = n1 - n2
        ElseIf (Signo = 42) Then 'Multiplicacion
            OperacionesBasicas = n1 * n2
        ElseIf (Signo = 47) Then 'División
            OperacionesBasicas = n1 / n2
        End If
    End Function
End Class
